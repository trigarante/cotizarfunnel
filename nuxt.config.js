

module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'Cotizar Seguro de Auto - Cotizador de Seguros - AhorraSeguros.Mx®',
    meta: [
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1'},
      {hid: 'description',name: 'description',content: 'Cotizar Seguro de auto al mejor precio en AhorraSeguros®. Compara 20 Aseguradoras en 30 Segundos. Cotiza tu Seguro de auto en el Mejor Cotizador de Seguros.'},
      {hid: 'keywords',name: 'keywords',content: 'Cotizar Seguro de Auto, Cotizar Seguros de Autos, Cotiza tu seguro de auto, cotiza seguros de autos, cotizador de seguros de autos, cotizador de seguros para autos'},
      {hid: 'author', name: 'author', content: '@vargasdesignmx | @dCuevas'},
      {hid: 'robots', name: 'robots', content: 'index, follow'}
    ],
    link: [
      {rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Open+Sans:400,700'},
      {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'},
      {rel: 'sitemap', type: 'application/xml', href: '/sitemap.xml'},
      {rel: 'canonical', href: 'https://ahorraseguros.mx/seguros-de-autos/cotizar-seguro-de-auto/'},
    ],
  },
  /*
  ** Customize the progress bar color
  */
  loading: {
    name: 'cube-grid',
    color: '#30c13e',
    background: 'white'
  },
  /*
  ** Build configuration
  */
  build: {
    vendor: ['jquery', 'bootstrap'],
    /*
    ** Run ESLint on save
    */
    extend(config, {isDev, isClient}) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  },
  // include personalized css
  css: ['static/css/styles.css'],
  plugins: [{src: '~/plugins/filters.js', ssr: false}],
  modules: [
    'bootstrap-vue/nuxt',
    '@nuxtjs/font-awesome',
     ['nuxt-fontawesome', {
      component: 'fa',
       imports: [

        {
          set: '@fortawesome/free-solid-svg-icons',
          icons: ['fas']
        },
    ],
    }],
    ['@nuxtjs/google-tag-manager', {
      id: 'GTM-KRCFZQ'
      // layer: 'dataLayer',
      // pageTracking: 'true'
    }],
    ['@nuxtjs/sitemap'],
    ['nuxt-validate', {
      lang: 'es'
    }]
  ],
  env: {
    urlWSAutos: 'https://ahorraseguros.mx/ws-autos/servicios',
    urlDB: 'https://ahorraseguros.mx/ws-rest/servicios',
    //urlDB: 'http://192.168.10.61:8080/ws-rest/servicios',
    urlNewDataBase: 'https://www.ahorraseguros.mx/servicios',
    //urlNewDataBase: 'http://192.168.10.65:8090/servicios'
    //NODE_ENV: 'production'  
  },
  router: {
      base: '/seguros-de-autos/cotizar-seguro-de-auto/'
    },
  render: {
    http2: {push: true},
    resourceHints: false,
    gzip: {threshold: 9}
  },

}
