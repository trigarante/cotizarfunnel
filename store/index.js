import Vuex from 'vuex'

import cotizacionService from '~/plugins/cotizacion'

const createStore = () => {
  return new Vuex.Store({
    state: {
      ambientePruebas:false,
      control: {
        vista: false,
        modalAseguradora: '0',
      },
      config: {
        telefonoPrincipal: '47440410',
        telefonoSecundario: '',
        grupoCallback: 'VN GNP',
        from: '',
        idPagina: 90,
        idCampana: 2,
        loading: false,
        ordenDirigido: true,
        numCotizacionUsuario: 0,
        ipCliente: '127.0.0.1',
        idEjecutivo: 0,
        cobertura: {
          amplia_premium: 1,
          amplia_clasica: 2,
          amplia_ahorra: 3,
          amplia_esencial: 4,
          amplia_limitada: 5,
          rc: 6,
        }
      },
     aseguradoras: {
        'ABA': {
          nombre: 'ABA',
          //logo_aseg:'ABA',
          id_aseguradora: 1,
          id_pagina: 1080,
          id_campana: 4,
          callback: "VN ABA",
          DID: "5547440407",
          cotizacion: true,
          emision: false,
          descuento: 0,
          calificacion: '5.0',
          phonecotizar: '(55)88510148',
          horario_atencion:'L-V 8 a 21 hrs. / S 9 a 14 hrs.',
          stars: '★★★★★',
          popularidad: 'Muy popular',
          promo: '20% de desc de contado',
          textogenerico: 'Este es el mejor precio para tu seguro',
          textbadget: '20% de desc de contado',
          text_poppers: 'seguros aba',
          estatus_cotizacion: false,
          respuestaCotizacion: {},
          respuestaEmision: {},
          btn: false,
          detallesAseguradora: [],
          diferenciadores: [
            {
              d1: 'Diferenciador 1'
            },
            {
              d2: 'Diferenciador 2'
            },
            {
              d3: 'Diferenciador 3'
            }
          ],
          orden: 0
        },
        'ABA2': {
      nombre: 'ABA2',
      //logo_aseg:'ABA2',
      id_aseguradora: 1,
      id_pagina: 1080,
      id_campana: 4,
      callback: "VN ABA",
      DID: "5547440407",
      cotizacion: true,
      emision: false,
      descuento: 0,
      calificacion: '5.0',
      phonecotizar: '(55)88510148',
      horario_atencion:'L-V 8 a 21 hrs. / S 9 a 14 hrs.',
      stars: '★★★★★',
      popularidad: 'Muy popular',
      promo: '20% de desc de contado',
      textogenerico: 'Este es el mejor precio para tu seguro',
      textbadget: '20% de desc de contado',
      text_poppers: 'seguros aba',
      estatus_cotizacion: false,
      respuestaCotizacion: {},
      respuestaEmision: {},
      btn: false,
      detallesAseguradora: [],
      diferenciadores: [
      {
        d1: 'Diferenciador 1'
      },
      {
        d2: 'Diferenciador 2'
      },
      {
        d3: 'Diferenciador 3'
      }
    ],
      orden: 0
  },
        'ANA': {
          nombre: 'ANA',
          //logo_aseg:'ANA',
          id_aseguradora: 4,
          id_pagina: 1086,
          id_campana: 44,
          callback: "VN ANA",
          DID: "5547495119",
          cotizacion: false,
          emision: false,
          descuento: 25,
          calificacion: '4.9',
          phonecotizar: '(55)47495119',
          horario_atencion:'L-V 8 a 21 hrs. / S 9 a 14 hrs.',
          stars: '★★★★★',
          popularidad: 'Muy Popular',
          promo: '25% de desc de contado y 12 MSI',
          textogenerico: 'Este es el mejor precio para tu seguro',
          textbadget: '25% de desc de contado y 12 MSI',
          //alttext: 'ANA',
          text_poppers: 'seguros ana',
          estatus_cotizacion: false,
          respuestaCotizacion: {},
          respuestaEmision: {},
          btn: false,
          detallesAseguradora: [],
          diferenciadores:
            [
              {
                d1: 'Diferenciador 1'
              },
              {
                d2: 'Diferenciador 2'
              },
              {
                d3: 'Diferenciador 3'
              }
            ],
          orden:
            0
        },
        'ATLAS': {
          nombre: 'ATLAS',
          //logo_aseg: 'ATLAS',
          id_aseguradora: 5,
          id_pagina: 1088,
          id_campana: 114,
          callback: "VN ATLAS",
          DID: "5547495143",
          cotizacion: true,
          emision: true,
          descuento: 30,
          calificacion: '4.8',
          phonecotizar: '(55)47495143',
          horario_atencion:'L-V 8 a 21 hrs. / S 9 a 14 hrs.',
          stars: '★★★★★',
          popularidad: 'Muy Popular',
          promo: 'Obten 37% de Descuento',
          textogenerico: 'Este es el mejor precio para tu seguro',
          textbadget: 'Obten 37% de Descuento',
          //alttext: 'ATLAS',
          text_poppers: 'seguros atlas',
          estatus_cotizacion: false,
          respuestaCotizacion: {},
          respuestaEmision: {},
          btn: false,
          detallesAseguradora: [],
          diferenciadores:
            [
              {
                d1: 'Diferenciador 1'
              },
              {
                d2: 'Diferenciador 2'
              },
              {
                d3: 'Diferenciador 3'
              }
            ],
          orden:
            0
        },
        'AXA': {
          nombre: 'AXA',
          //logo_aseg: 'AXA',
          id_aseguradora: 6,
          id_pagina: 1090,
          id_campana: 10,
          callback: "VN AXA",
          DID: "5547495115",
          cotizacion: true,
          emision: false,
          descuento: 0,
          calificacion: '4.7',
          phonecotizar: '(55)47495115',
          horario_atencion:'L-V 8 a 21 hrs. / S 9 a 14 hrs.',
          stars: '★★★★★',
          popularidad: 'Muy popular',
          promo: '12 Meses sin Intereses',
          textogenerico: 'Este es el mejor precio para tu seguro',
          textbadget: 'Consigue mejor precio por teléfono',
          //alttext: 'AXA',
          text_poppers: 'seguros axa',
          estatus_cotizacion: false,
          respuestaCotizacion: {},
          respuestaEmision: {},
          btn: false,
          detallesAseguradora: [],
          diferenciadores:
            [
              {
                d1: 'Diferenciador 1'
              },
              {
                d2: 'Diferenciador 2'
              },
              {
                d3: 'Diferenciador 3'
              }
            ],
          orden:
            0
        },
        'BANORTE': {
          nombre: 'BANORTE',
          //logo_aseg: 'BANORTE',
          id_aseguradora: 7,
          id_pagina: 1094,
          id_campana: 11,
          callback: "VN BANORTE",
          DID: "5547495120",
          cotizacion: true,
          emision: false,
          descuento: 0,
          calificacion: '4.6',
          phonecotizar: '(55)47495120',
          horario_atencion:'L-V 8 a 21 hrs. / S 9 a 14 hrs.',
          stars: '★★★★☆',
          popularidad: 'Popular',
          promo: 'Un asesor te llamara para obtener hasta un 35% desc',
          textogenerico: 'Este es el mejor precio para tu seguro',
          textbadget: 'Un asesor te llamara para obtener hasta un 35% desc',
          text_poppers: 'seguros banorte',
          estatus_cotizacion: false,
          respuestaCotizacion: {},
          respuestaEmision: {},
          btn: false,
          detallesAseguradora: [],
          diferenciadores:
            [
              {
                d1: 'Diferenciador 1'
              },
              {
                d2: 'Diferenciador 2'
              },
              {
                d3: 'Diferenciador 3'
              }
            ],
          orden:
            0
        },
        'GNP': {
          nombre: 'GNP',
          //logo_aseg: 'GNP',
          id_aseguradora: 11,
          id_pagina: 1099,
          id_campana: 2,
          callback: "VN GNP",
          DID: "5547495116",
          cotizacion: true,
          emision: false,
          descuento: 0,
          calificacion: '4.5',
          phonecotizar: '(55)47495116',
          horario_atencion:'L-V 8 a 21 hrs. / S 9 a 14 hrs.',
          stars: '★★★★★',
          popularidad: 'Muy popular',
          promo: 'Obten 5% desc y paga 3 6 o 12 MSI',
          textogenerico: 'Este es el mejor precio para tu seguro',
          textbadget: 'Obten 5% desc y paga 3 6 o 12 MSI',
          text_poppers: 'seguros gnp',
          estatus_cotizacion: false,
          respuestaCotizacion: {},
          respuestaEmision: {},
          btn: false,
          detallesAseguradora: [],
          diferenciadores:
            [
              {
                d1: 'Diferenciador 1'
              },
              {
                d2: 'Diferenciador 2'
              },
              {
                d3: 'Diferenciador 3'
              }
            ],
          orden:
            0
        },
        'HDI': {
          nombre: 'HDI',
          //logo_aseg: 'HDI',
          id_aseguradora: 12,
          id_pagina: 1124,
          id_campana: 17,
          callback: "VN HDI",
          DID: "5547495127",
          cotizacion: true,
          emision: false,
          descuento: 10,
          calificacion: '5.0',
          phonecotizar: '(55)88510149',
          horario_atencion:'L-V 8 a 21 hrs. / S 9 a 14 hrs.',
          stars: '★★★★★',
          popularidad: 'Muy popular',
          promo: 'Recibe 25% de desc y Paga a 12 MSI',
          textogenerico: 'Este es el mejor precio para tu seguro',
          textbadget: 'Recibe 25% de desc y Paga a 12 MSI',
          text_poppers: 'seguros hdi',
          estatus_cotizacion: false,
          respuestaCotizacion: {},
          respuestaEmision: {},
          btn: false,
          detallesAseguradora: [],
          diferenciadores:
            [
              {
                d1: 'Diferenciador 1'
              },
              {
                d2: 'Diferenciador 2'
              },
              {
                d3: 'Diferenciador 3'
              }
            ],
          orden:
            0
        },
        'HDI2': {
          nombre: 'HDI2',
          //logo_aseg: 'HDI',
          id_aseguradora: 12,
          id_pagina: 1124,
          id_campana: 17,
          callback: "VN HDI",
          DID: "5547495127",
          cotizacion: true,
          emision: false,
          descuento: 0,
          calificacion: '5.0',
          phonecotizar: '(55)88510149',
          horario_atencion:'L-V 8 a 21 hrs. / S 9 a 14 hrs.',
          stars: '★★★★★',
          popularidad: 'Muy popular',
          promo: 'Recibe 25% de desc y Paga a 12 MSI',
          textogenerico: 'Este es el mejor precio para tu seguro',
          textbadget: 'Recibe 25% de desc y Paga a 12 MSI',
          text_poppers: 'seguros hdi',
          estatus_cotizacion: false,
          respuestaCotizacion: {},
          respuestaEmision: {},
          btn: false,
          detallesAseguradora: [],
          diferenciadores:
            [
              {
                d1: 'Diferenciador 1'
              },
              {
                d2: 'Diferenciador 2'
              },
              {
                d3: 'Diferenciador 3'
              }
            ],
          orden:
            0
        },
        'QUALITAS': {
          nombre: 'QUALITAS',
          //logo_aseg: 'QUALITAS',
          id_aseguradora: 18,
          id_pagina: 1089,
          id_campana: 1,
          callback: "VN QUALITAS",
          DID: "5547495114",
          cotizacion: true,
          emision: true,
          descuento: 20,
          calificacion: '5.0',
          phonecotizar: '(55)47495114',
          horario_atencion:'L-V 8 a 21 hrs. / S 9 a 14 hrs.',
          stars: '★★★★★',
          popularidad: 'Muy popular',
          promo: 'Recibe 20% de desc y Paga a 3 y 6 MSI.',
          textogenerico: 'Este es el mejor precio para tu seguro',
          textbadget: 'Recibe 20% de desc y Paga a 3 y 6 MSI.',
          text_poppers: 'seguros qualitas',
          estatus_cotizacion: false,
          respuestaCotizacion: {},
          respuestaEmision: {},
          btn: false,
          habilitar:false,
          detallesAseguradora: [],
          diferenciadores:
            [
              {
                d1: 'Diferenciador 1'
              },
              {
                d2: 'Diferenciador 2'
              },
              {
                d3: 'Diferenciador 3'
              }
            ],
          orden:
            0
        },
        'MAPFRE': {
          nombre: 'MAPFRE',
          //logo_aseg: 'MAPFRE',
          id_aseguradora: 15,
          id_pagina: 1096,
          id_campana: 106,
          callback: "VN Mapfre",
          DID: "5547495111",
          cotizacion: true,
          emision: true,
          descuento: 0,
          calificacion: '4.2',
          phonecotizar: '(55)47495111',
          horario_atencion:'L-V 8 a 21 hrs. / S 9 a 14 hrs.',
          stars: '★★★★☆',
          popularidad: 'Popular',
          promo: '12 Meses sin Intereses',
          textogenerico: 'Este es el mejor precio para tu seguro',
          textbadget: 'Consigue mejor precio por teléfono',
          text_poppers: 'seguros mapfre',
          estatus_cotizacion: true,
          respuestaCotizacion: {},
          respuestaEmision: {},
          btn: false,
          detallesAseguradora: [],
          diferenciadores:
            [
              {
                d1: 'Diferenciador 1'
              },
              {
                d2: 'Diferenciador 2'
              },
              {
                d3: 'Diferenciador 3'
              }
            ],
          orden:
            0
        },
        'ELAGUILA': {
          nombre: 'ELAGUILA',
          //logo_aseg: 'ELAGUILA',
          id_aseguradora: 8,
          id_pagina: 1097,
          id_campana: 12,
          callback: "VN EL AGUILA",
          DID: "5547495125",
          cotizacion: true,
          emision: false,
          descuento: 5,
          calificacion: '4.1',
          phonecotizar: '(55)88510147',
          horario_atencion:'L-V 9 a.m. a 7 p.m. y S 9 a.m. a 1 p.m.',
          stars: '★★★★☆',
          popularidad: 'Popular',
          promo: 'Recibe 15% de desc y Paga a 12 MSI.',
          textogenerico: 'Este es el mejor precio para tu seguro',
          textbadget: 'Recibe 15% de desc y Paga a 12 MSI.',
          text_poppers: 'seguros el aguila',
          estatus_cotizacion: true,
          respuestaCotizacion: {},
          respuestaEmision: {},
          btn: false,
          detallesAseguradora: [],
          diferenciadores:
            [
              {
                d1: 'Diferenciador 1'
              },
              {
                d2: 'Diferenciador 2'
              },
              {
                d3: 'Diferenciador 3'
              }
            ],
          orden: 0
        },
        'SURA': {
          nombre: 'SURA',
          //logo_aseg: 'SURA',
          id_aseguradora: 33,
          id_pagina: 1087,
          id_campana: 43,
          callback: "VN SURA",
          DID: "5528814666",
          cotizacion: true,
          emision: false,
          descuento: 30,
          calificacion: '4.0',
          phonecotizar: '(55) 88801532',
          horario_atencion:'L-V 8 a 21 hrs. / S 9 a 14 hrs.',
          stars: '★★★★☆',
          promo: 'Recibe 30% de desc y Paga a 6 MSI.',
          textogenerico: 'Este es el mejor precio para tu seguro',
          textbadget: 'Recibe 30% de desc y Paga a 6 MSI.',
          text_poppers: 'seguros sura',
          estatus_cotizacion: false,
          respuestaCotizacion: {},
          respuestaEmision: {},
          btn: false,
          detallesAseguradora: [],
          diferenciadores:
            [
              {
                d1: 'Diferenciador 1'
              },
              {
                d2: 'Diferenciador 2'
              },
              {
                d3: 'Diferenciador 3'
              }
            ],
          orden: 0
        },
        'ZURICH': {
          nombre: 'ZURICH',
          //logo_aseg: 'ZURICH',
          id_aseguradora: 20,
          id_pagina: 1081,
          id_campana: 31,
          callback: "VN ZURICH",
          DID: "5547495130",
          cotizacion: false,
          emision: false,
          descuento: 0,
          calificacion: '4.0',
          phonecotizar: '(55)47495130',
          horario_atencion:'L-V 8 a 21 hrs. / S 9 a 14 hrs.',
          stars: '★★★★☆',
          popularidad: 'Popular',
          promo: 'Paga a 3 y 6 MSI con tarjetas participantes',
          textogenerico: 'Este es el mejor precio para tu seguro',
          textbadget: 'Paga a 3 y 6 MSI con tarjetas participantes',
          text_poppers: 'seguros zurich',
          estatus_cotizacion: false,
          respuestaCotizacion: {},
          respuestaEmision: {},
          btn: false,
          detallesAseguradora: [],
          diferenciadores:
            [
              {
                d1: 'Diferenciador 1'
              },
              {
                d2: 'Diferenciador 2'
              },
              {
                d3: 'Diferenciador 3'
              }
            ],
          orden: 0
        },
        'AFIRME': {
          nombre: 'AFIRME',
          //logo_aseg: 'AFIRME',
          id_aseguradora: 2,
          id_pagina: 1082,
          id_campana: 43,
          callback: "VN Multimarcas",
          DID: "5547495142",
          cotizacion: true,
          emision: false,
          descuento: 10,
          calificacion: '4.0',
          phonecotizar: '(55)47495142',
          horario_atencion:'L-V 8 a 21 hrs. / S 9 a 14 hrs.',
          stars: '★★★★☆',
          popularidad: 'Popular',
          promo: 'Recibe 40% de desc y paga a 12 MSI con tarjetas participantes',
          textogenerico: 'Este es el mejor precio para tu seguro',
          textbadget: 'Recibe 40% de desc y paga a 12 MSI',
          text_poppers: 'seguros afirme',
          estatus_cotizacion: false,
          respuestaCotizacion: {},
          respuestaEmision: {},
          btn: false,
          detallesAseguradora: [],
          diferenciadores:
            [
              {
                d1: 'Diferenciador 1'
              },
              {
                d2: 'Diferenciador 2'
              },
              {
                d3: 'Diferenciador 3'
              }
            ],
          orden: 0
        },
        'AIG': {
          nombre: 'AIG',
          //logo_aseg: 'AIG',
          id_aseguradora: 3,
          id_pagina: 1085,
          id_campana: 6,
          callback: "VN Multimarcas",
          DID: "5528814666",
          cotizacion: true,
          emision: false,
          descuento: 0,
          calificacion: '4.0',
          phonecotizar: '(55)28814666',
          horario_atencion:'L-V 8 a 21 hrs. / S 9 a 14 hrs.',
          stars: '★★★★☆',
          popularidad: 'Popular',
          promo: 'Paga a 12 MSI con tarjetas participantes',
          textogenerico: 'Este es el mejor precio para tu seguro',
          textbadget: 'Paga a 12 MSI con tarjetas participantes',
          text_poppers: 'seguros aig',
          estatus_cotizacion: false,
          respuestaCotizacion: {},
          respuestaEmision: {},
          btn: false,
          detallesAseguradora: [],
          diferenciadores:
            [
              {
                d1: 'Diferenciador 1'
              },
              {
                d2: 'Diferenciador 2'
              },
              {
                d3: 'Diferenciador 3'
              }
            ],
          orden: 0
        },
        'ELPOTOSI': {
          nombre: 'ELPOTOSI',
          //logo_aseg: 'ELPOTOSI',
          id_aseguradora: 9,
          id_pagina: 1101,
          id_campana: 43,
          callback: "VN Multimarcas",
          DID: "5547495143",
          cotizacion: true,
          emision: false,
          descuento: 25,
          calificacion: '4.0',
          phonecotizar: '(55)88801534',
          horario_atencion:'L-V 8 a 21 hrs. / S 9 a 14 hrs.',
          stars: '★★★★☆',
          popularidad: 'Popular',
          promo: 'Obten 20% desc y paga a 6 MSI con tarjetas participantes',
          textogenerico: 'Este es el mejor precio para tu seguro',
          textbadget: 'Obten 20% desc y paga a 6 MSI',
          text_poppers: 'seguros el potosi',
          estatus_cotizacion: false,
          respuestaCotizacion: {},
          respuestaEmision: {},
          btn: false,
          detallesAseguradora: [],
          diferenciadores:
            [
              {
                d1: 'Diferenciador 1'
              },
              {
                d2: 'Diferenciador 2'
              },
              {
                d3: 'Diferenciador 3'
              }
            ],
          orden: 0
        },
        'GENERALDESEGUROS': {
          nombre: 'GENERALDESEGUROS',
          //logo_aseg: 'GENERALDESEGUROS',
          id_aseguradora: 10,
          id_pagina: 1102,
          id_campana: 43,
          callback: "VN Multimarcas",
          DID: "5588801536",
          cotizacion: true,
          emision: false,
          descuento: 25,
          calificacion: '4.0',
          phonecotizar: '(55)88801536',
          horario_atencion:'L-V 8 a 21 hrs. / S 9 a 14 hrs.',
          stars: '★★★★☆',
          popularidad: 'Popular',
          promo: 'Recibe 25% de Descuento y Paga a 6 MSI',
          textogenerico: 'Este es el mejor precio para tu seguro',
          textbadget: 'Recibe 25% de Descuento y Paga a 6 MSI',
          text_poppers: 'general de seguros',
          estatus_cotizacion: false,
          respuestaCotizacion: {},
          respuestaEmision: {},
          btn: false,
          detallesAseguradora: [],
          diferenciadores:
            [
              {
                d1: 'Diferenciador 1'
              },
              {
                d2: 'Diferenciador 2'
              },
              {
                d3: 'Diferenciador 3'
              }
            ],
          orden: 0
        },
        'INBURSA': {
          nombre: 'INBURSA',
          //logo_aseg: 'INBURSA',
          id_aseguradora: 13,
          id_pagina: 1148,
          id_campana: 18,
          callback: "VN INBURSA",
          DID: "5547495128",
          cotizacion: false,
          emision: false,
          descuento: 0,
          calificacion: '4.0',
          phonecotizar: '(55)47495128',
          horario_atencion:'L-V 8 a 21 hrs. / S 9 a 14 hrs.',
          stars: '★★★★☆',
          popularidad: 'Popular',
          promo: '6 MSI con tarjetas Inbursa',
          textogenerico: '6 MSI con tarjetas Inbursa',
          textbadget: '6 MSI con tarjetas Inbursa',
          text_poppers: 'seguros inbursa',
          estatus_cotizacion: false,
          respuestaCotizacion: {},
          respuestaEmision: {},
          btn: false,
          detallesAseguradora: [],
          diferenciadores:
            [
              {
                d1: 'Diferenciador 1'
              },
              {
                d2: 'Diferenciador 2'
              },
              {
                d3: 'Diferenciador 3'
              }
            ],
          orden: 0
        },
        'LALATINO': {
          nombre: 'LALATINO',
          //logo_aseg: 'LALATINO',
          id_aseguradora: 14,
          id_pagina: 1100,
          id_campana: 19,
          callback: "VN Multimarcas",
          DID: "5547495118",
          cotizacion: false,
          emision: false,
          descuento: 0,
          calificacion: '4.0',
          phonecotizar: '(55)47495118',
          horario_atencion:'L-V 8 a 21 hrs. / S 9 a 14 hrs.',
          stars: '★★★★☆',
          popularidad: 'Popular',
          promo: 'Recibe 30% de Descuento y Paga a 12 MSI.',
          textogenerico: 'Este es el mejor precio para tu seguro',
          textbadget: 'Recibe 30% de Descuento y Paga a 12 MSI.',
          text_poppers: 'la latino seguros',
          estatus_cotizacion: false,
          respuestaCotizacion: {},
          respuestaEmision: {},
          btn: false,
          detallesAseguradora: [],
          diferenciadores:
            [
              {
                d1: 'Diferenciador 1'
              },
              {
                d2: 'Diferenciador 2'
              },
              {
                d3: 'Diferenciador 3'
              }
            ],
          orden: 0
        },
        'PRIMEROSEGUROS': {
          nombre: 'PRIMEROSEGUROS',
          //logo_aseg: 'PRIMEROSEGUROS',
          id_aseguradora: 17,
          id_pagina: 1161,
          id_campana: 43,
          callback: "VN Multimarcas",
          DID: "5588801535",
          cotizacion: false,
          emision: false,
          descuento: 0,
          calificacion: '4.0',
          phonecotizar: '(55)88801535',
          horario_atencion:'L-V 8 a 21 hrs. / S 9 a 14 hrs.',
          stars: '★★★★☆',
          popularidad: 'Popular',
          promo: 'Ahorra hasta * 30%',
          textogenerico: 'Este es el mejor precio para tu seguro',
          textbadget: 'Consigue mejor precio por teléfono',
          text_poppers: 'primero seguros',
          estatus_cotizacion: false,
          respuestaCotizacion: {},
          respuestaEmision: {},
          btn: false,
          diferenciadores:
            [
              {
                d1: 'Diferenciador 1'
              },
              {
                d2: 'Diferenciador 2'
              },
              {
                d3: 'Diferenciador 3'
              }
            ],
          orden: 0
        },
        'BXMAS': {
          nombre: 'BXMAS',
          //logo_aseg: 'BXMAS',
          id_aseguradora: 16,
          id_pagina: 1083,
          id_campana: 43,
          callback: "VN Multimarcas",
          DID: "5547440407",
          cotizacion: true,
          emision: false,
          descuento: 30,
          calificacion: '4.0',
          phonecotizar: '(55)47440412',
          horario_atencion:'L-V 8 a 21 hrs. / S 9 a 14 hrs.',
          stars: '★★★★☆',
          popularidad: 'Popular',
          promo: 'Recibe 30% de Descuento',
          textogenerico: 'Este es el mejor precio para tu seguro',
          textbadget: 'Recibe 30% de Descuento',
          text_poppers: 'seguros bxmas',
          estatus_cotizacion: false,
          respuestaCotizacion: {},
          respuestaEmision: {},
          btn: false,
          detallesAseguradora: [],
          diferenciadores:
            [
              {
                d1: 'Diferenciador 1'
              },
              {
                d2: 'Diferenciador 2'
              },
              {
                d3: 'Diferenciador 3'
              }
            ],
          orden: 0
        }
      },
      ejecutivo: {
        nombre: '',
        correo: '',
        id: 0
      },
      formCotizacion: {
        marca: '',
        modelo: '',
        descripcion: '',
        subDescripcion: '',
        detalle: '',
        detalleid: '',
        codigoPostal: '',
        nombre: '',
        apellidoPaterno: '',
        apellidoMaterno: '',
        telefono: '',
        correo: '',
        edad: '',
        fechaNacimiento: '',
        genero: '',
        colonias: [],
        bancos: [],
      },
      cotizaciones: [],
      emision: {

        "Aseguradora": "MAPFRE",
        "Descuento": "0",
        "Cliente": {
          "TipoPersona": "F",
          "Nombre": "BRANDON",
          "ApellidoPat": "GUADARRAMA",
          "ApellidoMat": "DURAN",
          "RFC": "GUDB000104000",
          "FechaNacimiento": "04/01/2000",
          "Ocupacion": null,
          "CURP": null,
          "Direccion": {
            "Calle": "NORTE 94",
            "NoExt": "101",
            "NoInt": "8019",
            "Colonia": "La Esmeralda",
            "CodPostal": "07540",
            "Poblacion": "Gustavo A. Madero",
            "Ciudad": "Distrito Federal",
            "Pais": "MEXICO"
          },
          "Edad": 18,
          "Genero": "MASCULINO",
          "Telefono": "5529621064",
          "Email": "seguimiento@ahorraseguros.mx"
        },
        "Vehiculo": {
          "Uso": "PARTICULAR",
          "Marca": "CHEVROLET",
          "Modelo": "2015",
          "NoMotor": "ASD654A6S6",
          "NoSerie": "4VG7DARJ7XN769197",
          "NoPlacas": "ASD654",
          "Descripcion": "SPARK 1.2 PAQ.G",
          "CodMarca": null,
          "CodDescripcion": null,
          "CodUso": null,
          "Clave": "16621501",
          "Servicio": "PARTICULAR"
        },
        "Coberturas": [],
        "Paquete": "AMPLIA",
        "PeriodicidadDePago": 0,
        "Emision": {
          "PrimaTotal": null,
          "PrimaNeta": null,
          "Derechos": null,
          "Impuesto": null,
          "Recargos": null,
          "PrimerPago": null,
          "PagosSubsecuentes": null,
          "IDCotizacion": null,
          "Terminal": null,
          "Documento": null,
          "Poliza": null,
          "Resultado": null
        },
        "Pago": {
          "MedioPago": "CREDITO",
          "NombreTarjeta": "BRANDON GUADARRAMA",
          "Banco": "BANAMEX",
          "NoTarjeta": "5445321321321321",
          "MesExp": "07",
          "AnioExp": "2023",
          "CodigoSeguridad": "123",
          "NoClabe": null,
          "Carrier": 0
        },
        "CodigoError": null,
        "urlRedireccion": "http://192.168.11.1:8081/fed2/pago-exitoso-terminal.xhtml",
        "Cotizacion": {
          "PrimaTotal": "8798.99",
          "PrimaNeta": "7125.34",
          "Derechos": "460",
          "Impuesto": "1213.65",
          "Recargos": "0",
          "PrimerPago": "8798.99",
          "PagosSubsecuentes": "8798.99",
          "IDCotizacion": null,
          "CotID": null,
          "VerID": null,
          "CotIncID": null,
          "VerIncID": null,
          "Resultado": "True"
        }
      },

    }
    ,
    mutations: {
      reCotizarAseguradoraStore(state, attributesRecotizacion) {



        console.log(attributesRecotizacion);
        var aseguradoraRecotizacion = '';

        var aseguradoraNodoRecotizacion = '';
        aseguradoraRecotizacion = attributesRecotizacion.aseguradoraRecotizacion;

        aseguradoraNodoRecotizacion = attributesRecotizacion.aseguradoraNodoRecotizacion;
        cotizacionService.search(
          attributesRecotizacion.aseguradoraRecotizacion,
          attributesRecotizacion.selectedDetalleId,
          state.formCotizacion.codigoPostal,
          attributesRecotizacion.selectedDetalleText,
          state.aseguradoras[attributesRecotizacion.aseguradoraRecotizacion].descuento,
          state.formCotizacion.edad,
          state.formCotizacion.fechaNacimiento,
          state.formCotizacion.genero,
          state.formCotizacion.marca,
          state.formCotizacion.modelo,
          'cotizacion',
          'AMPLIA',
          'PARTICULAR')
          .then(resp => {

            if (typeof resp != "string") {
              if (Object.keys(resp.Cotizacion).includes('PrimaTotal') && resp.Cotizacion.PrimaTotal != null && resp.Cotizacion.PrimaTotal != '') {
                if (resp.Coberturas.length > 0) {

                  resp.Show = state.cotizaciones[aseguradoraNodoRecotizacion].Show;
                  resp.Calificacion = state.cotizaciones[aseguradoraNodoRecotizacion].Calificacion;
                  resp.configuracionPeticion = state.cotizaciones[aseguradoraNodoRecotizacion].configuracionPeticion;

                  //Se eliminan caracteres del presio para que pueda ser ardenado sin problemas
                  resp.Cotizacion.PrimaTotal=resp.Cotizacion.PrimaTotal.replace("$","");
                  resp.Cotizacion.PrimaTotal=resp.Cotizacion.PrimaTotal.replace(",","");
                  //Se eliminan coberturas sin informacion
                  if (resp.Coberturas) {
                    for (var c in resp.Coberturas[0]) {
                      if (resp.Coberturas[0][c] === "-") {
                        delete resp.Coberturas[0][c];
                      }
                    }
                  }

                  state.cotizaciones[aseguradoraNodoRecotizacion] = resp;
                  state.cotizaciones[aseguradoraNodoRecotizacion].Show=true;
                  //Vue.set(state,state.cotizaciones[aseguradoraNodoRecotizacion].Show,true);
                  //this.resultDatosTabla = Object.assign({}, this.$store.state.cotizaciones[aseguradoraNodoRecotizacion], resp)

                  /*SE ORDENAN LAS COTIZACIONES DE FORMA ASCENDENTE*/
                  state.cotizaciones.sort(function(a,b){
                    return a.Cotizacion.PrimaTotal - b.Cotizacion.PrimaTotal;
                  });

                } else {
                  console.log(aseguradora + " no trajo coberturas D:");
                }
              } else {
                console.log(aseguradora + " no trajo precios D:");
              }
            } else {
              console.log("Problema al cotizar con " + aseguradoraRecotizacion);
            }

          })





      },


    }

  })
}


export default createStore
