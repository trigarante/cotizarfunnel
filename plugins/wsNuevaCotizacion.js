import axios from 'axios'


const statusNewCotizacion = {}

statusNewCotizacion.nuevaCotizacion = function (peticion) {

  return axios({
    method: "post",
    url: process.env.urlNewDataBase + '/saveCotizacion',
    data: JSON.parse(peticion)
  })
    .then(res => res.data)
    .catch(err => console.error("Ups... no se pudo guardar la nueva cotizacion:( "+err));
}
export default statusNewCotizacion


