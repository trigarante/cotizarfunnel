import axios from 'axios'


const cotizacionService = {}

cotizacionService.search = function (aseguradora,
                                     clave,
                                     cp,
                                     descripcion,
                                     descuento,
                                     edad,
                                     fechaNacimiento,
                                     genero,
                                     marca,
                                     modelo,
                                     movimiento,
                                     paquete,
                                     servicio) {

  return axios({
    method: "post",
    url: process.env.urlWSAutos + '/cotizar',
    data: {
      aseguradora,
      clave,
      cp,
      descripcion,
      descuento,
      edad,
      fechaNacimiento,
      genero,
      marca,
      modelo,
      movimiento,
      paquete,
      servicio
    }
  })
    .then(res => res.data)
    .catch(err => console.error("Ups... "+aseguradora+" no pudo cotizar :( "+err));
}
export default cotizacionService


