import autosService from './ws-autos'

const detallesService ={}

detallesService.search=function (aseguradora, marca, modelo, descripcion,subdescripcion) {
  return autosService.get('/detallesAseguradora',{
    params:{aseguradora,marca,modelo,descripcion,subdescripcion}
  })
    .then(res => res.data)
    .catch(err => console.error("Ups... hubo un problema al obtener los detalles "+err));
}
export default detallesService
