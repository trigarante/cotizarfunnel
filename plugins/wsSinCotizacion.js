import axios from 'axios'


const statusSinCotizacion = {}

statusSinCotizacion.sinCotizacion = function (peticion) {

  return axios({
    method: "post",
    url: process.env.urlNewDataBase + '/saveSinCotizacion',
    data: JSON.parse(peticion)
  })
    .then(res => res.data)
    .catch(err => console.error("Ups... no se pudo guardar la nueva cotizacion:( "+err));
}
export default statusSinCotizacion


