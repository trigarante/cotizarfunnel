import autosService from './ws-autos'

const marcasService ={}

marcasService.search=function (aseguradora) {
  return autosService.get('/marcas',{
    params:{aseguradora}
  })
    .then(res => res.data)
    .catch(err => console.error("Ups... hubo un problema para obtener las marcas "+err));
}
export default marcasService



