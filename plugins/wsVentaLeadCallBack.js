import axios from 'axios'


const ventaLeadCallBack = {}

ventaLeadCallBack.leadCallBack = function (telefonoCliente) {

  return axios({
    method: "get",
    url: process.env.urlDB + '/ventaLeadCallbackElAguila?telefonoCliente='+telefonoCliente,
    data: JSON.parse(telefonoCliente)
  })
    .then(res => res.data)
    .catch(err => console.error("Ups... no se pudo guardar el lead  enviado :( "+err));
}
export default ventaLeadCallBack
