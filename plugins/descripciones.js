import autosService from './ws-autos'

const descripcionesService ={}

descripcionesService.search=function (aseguradora, marca, modelo) {
  return autosService.get('/descripciones',{
    params:{aseguradora,marca,modelo}
  })
    .then(res => res.data)
    .catch(err => console.error("Ups... hubo un problema al obtener las descripciones "+err));
}
export default descripcionesService
