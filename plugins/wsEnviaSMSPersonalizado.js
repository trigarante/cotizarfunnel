import axios from 'axios'


const wsSMSPersonalizado = {}

wsSMSPersonalizado.nuevoSMSPersonalizado = function (peticion) {

  return axios({
    method: "post",
    url: process.env.urlDB + '/sendSms',
    data: JSON.parse(peticion)
  })
    .then(res => res.data)
    .catch(err => console.error("Ups... no se pudo enviar el SMS personalizado:( "+err));
}
export default wsSMSPersonalizado
