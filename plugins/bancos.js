import autosWs from './ws-autos'

const bancosService ={}

bancosService.search=function (aseguradora) {
  return autosWs.get('/bancos',{
    params:{aseguradora}
  })
    .then(res => res.data)
    .catch(err => console.error("Ups... hubo un problema para obtener los Bancos "+err));
}
export default bancosService
