import axios from 'axios'


const newLeadAguila = {}

newLeadAguila.nuevoLeadAguila = function (peticion) {

  return axios({
    method: "post",
    url: process.env.urlNewDataBase + '/sendLeadElAguila',
    data: JSON.parse(peticion)
  })
    .then(res => res.data)
    .catch(err => console.error("Ups... no se pudo guardar el nuevo lid's :( "+err));
}
export default newLeadAguila


