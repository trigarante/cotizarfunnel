import autosService from './ws-autos'

const subdescripcionesService ={}

subdescripcionesService.search=function (aseguradora, marca, modelo, descripcion) {
  return autosService.get('/subdescripciones',{
    params:{aseguradora,marca,modelo,descripcion}
  })
    .then(res => res.data)
    .catch(err => console.error("Ups... hubo un problema al obtener las subdescripciones "+err));
}
export default subdescripcionesService
