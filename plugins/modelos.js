import autosService from './ws-autos'

const modelosService ={}

modelosService.search=function (aseguradora, marca) {
  return autosService.get('/modelos',{
    params:{aseguradora,marca}
  })
    .then(res => res.data)
    .catch(err => console.error("Ups... hubo un problema al obtener los modelos "+err));
}
export default modelosService
