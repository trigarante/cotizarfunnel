import axios from 'axios'


const newIdCotizacionGnp = {}

newIdCotizacionGnp.nuevoIdCotizacionGnp = function (peticion) {

  return axios({
    method: "post",
    url: process.env.urlNewDataBase + '/sendLeadGNP',
    data: JSON.parse(peticion)
  })
    .then(res => res.data)
    .catch(err => console.error("Ups... no se pudo al generar el id GNP:( "+err));
}
export default newIdCotizacionGnp


