import axios from 'axios'


const newLidGnp = {}
newLidGnp.nuevoLidGnp = function (peticion) {
  return axios({
    method: "post",
    url: process.env.urlNewDataBase + '/sendLeadGNP',
    data: JSON.parse(peticion)
  })
    .then(res => res.data)
    .catch(err => console.error("Ups... no se pudo guardar el nuevo leads GNP:( "+err));
}
export default newLidGnp


