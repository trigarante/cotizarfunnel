import axios from 'axios'


const datosDetalleVehiculo = {}

datosDetalleVehiculo.detalleVehiculo = function (peticion) {

  return axios.get(process.env.urlWSAutos + '/detallesAseguradora', {
    params: peticion
  })
    .then(res => res.data)
    .catch(err => console.error("Ups... no se obtener el detalle del vehiculo:( "+err));


}
export default datosDetalleVehiculo


