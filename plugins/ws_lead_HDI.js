import axios from 'axios'


const newLeadHDI = {}

newLeadHDI.nuevoLeadHdi = function (peticion) {

  return axios({
    method: "post",
    url: process.env.urlNewDataBase + '/sendLeadHDI',
    data: JSON.parse(peticion)
  })
    .then(res => res.data)
    .catch(err => console.error("Ups... no se pudo guardar el nuevo lead de HDI :( "+err));
}
export default newLeadHDI

