import axios from 'axios'


const newProspectService = {}

newProspectService.nuevoProspecto = function (peticion) {

  return axios({
    method: "post",
    url: process.env.urlNewDataBase + '/saveProspecto',
    data: JSON.parse(peticion)
  })
    .then(res => res.data)
    .catch(err => console.error("Ups... no se pudo guardar el nuevo prospecto:( "+err));
}
export default newProspectService


