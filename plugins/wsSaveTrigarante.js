import axios from 'axios'

const cotizacionService = {}

cotizacionService.search = function (anio,
                                     apellidoM,
                                     apellidoP,
                                     correo,
                                     cp,
                                     fechaNacimiento,
                                     grupoCallback,
                                     idCampana,
                                     idPagina,
                                     marca,
                                     nombre,
                                     sexo,
                                     submarca,
                                     telefono,
                                     telefonoAS,
                                     from,
                                     precio) {
  return axios({
    method: "post",
    url: process.env.urlDB + '/nuevaSolicitudAuto',
    data: {
      anio,
      apellidoM,
      apellidoP,
      correo,
      cp,
      fechaNacimiento,
      grupoCallback,
      idCampana,
      idPagina,
      marca,
      nombre,
      sexo,
      submarca,
      telefono,
      telefonoAS,
      from,
      precio
    }
  })
    .then(res => res.data)
    .catch(err => console.error(err));
}
export default cotizacionService

