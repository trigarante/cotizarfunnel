import axios from 'axios'


const emisionService = {}

emisionService.emitir = function (peticion) {

  return axios({
    method: "post",
    url: process.env.urlWSAutos + '/emitir',
    data: {peticion}
  })
    .then(res => res.data)
    .catch(err => console.error("Ups... "+aseguradora+" no pudo emitir :( "+err));
}
export default emisionService


