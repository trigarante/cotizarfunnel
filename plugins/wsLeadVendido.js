import axios from 'axios'


const wsLeadVendido = {}

wsLeadVendido.nuevoLeadVendido = function (peticion,idCotizacion_a) {

  return axios({
    method: "get",
    url: process.env.urlNewDataBase + '/leadVendido?idCotizacion='+peticion+'&idCotizacionA='+idCotizacion_a,
    data: JSON.parse(peticion)
  })
    .then(res => res.data)
    .catch(err => console.error("Ups... no se pudo guardar el lead  enviado :( "+err));
}
export default wsLeadVendido
