import axios from 'axios'


const newLeadAba = {}

newLeadAba.nuevoLeadAba = function (peticion) {

  return axios({
    method: "post",
    url: process.env.urlNewDataBase + '/sendLeadABA',
    data: JSON.parse(peticion)
  })
    .then(res => res.data)
    .catch(err => console.error("Ups... no se pudo guardar el nuevo lead de ABA :( "+err));
}
export default newLeadAba

