import baseWs from './ws-db'

const cpService ={}

cpService.search=function (cp) {
  return baseWs.get('/getAddress',{
    params:{cp}
  })
    .then(res => res.data)
    .catch(err => console.error("Ups... hubo un problema para obtener el CP "+err));
}
export default cpService
